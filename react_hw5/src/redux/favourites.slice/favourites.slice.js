import { createSlice } from "@reduxjs/toolkit";

const favouritesFromStorage = localStorage.getItem('favourites');
const initialFavourites = favouritesFromStorage ? JSON.parse(favouritesFromStorage) : [];

const initialState = {
    favourites: initialFavourites,
    favouritesCount: initialFavourites.length,
};

const favouritesSlice = createSlice({
    name: "favourites",
    initialState,
    reducers: {
        toggleFavourites: (state, { payload }) => {
            const existsIndex = state.favourites.findIndex(item => item.article === payload.article);
            if (existsIndex >= 0) {
                state.favourites.splice(existsIndex, 1); 
                state.favouritesCount -= 1;
            } else {
                state.favourites.push(payload); 
                state.favouritesCount += 1;
            }
            localStorage.setItem('favourites', JSON.stringify(state.favourites));
        },
    },
});

export const { toggleFavourites: toggleFavouritesAction } = favouritesSlice.actions;

export default favouritesSlice.reducer;


