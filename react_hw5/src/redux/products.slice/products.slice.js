import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

const initialState = {
    products: [],
    status: null
}

export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
    try {
        const response = await fetch("/products.json");
        if (!response.ok) {
            throw new Error('Failed to fetch products');
        }
        const data = await response.json();
        if (!data.products) {
            throw new Error('Products data not found in response');
        }
        return data.products;
    } catch (error) {
        console.error('Error fetching products:', error.message);
        throw error; 
    }
});

const productsSlice = createSlice({
    name: "products",
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchProducts.pending, (state) => {
                state.status = 'pending'
            })
            .addCase(fetchProducts.rejected, (state) => {
                state.status = 'error'
            })
            .addCase(fetchProducts.fulfilled, (state, action) => {
                state.status = 'done';
                state.products = action.payload;
            })
    }
});

export default productsSlice.reducer;
