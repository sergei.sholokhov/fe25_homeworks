import { configureStore, combineReducers} from '@reduxjs/toolkit';
// import { getDefaultMiddleware } from '@reduxjs/toolkit';
import productsReducer from './products.slice/products.slice';
import favouritesReducer from './favourites.slice/favourites.slice';
import cartItemsReducer from './cartItems.slice/cartItems.slice';
import modalReducer from './modal.slice/modal.slice';
import logger from 'redux-logger';

// import storage from 'redux-persist/lib/storage';
// import {
//     persistStore,
//     persistReducer,
//     FLUSH,
//     REHYDRATE,
//     PAUSE,
//     PERSIST,
//     PURGE,
//     REGISTER,
//   } from 'redux-persist';


const combinedReducer = combineReducers({
    products: productsReducer, // fetch product json from public folder
    modal: modalReducer, //modal isOpen what else?
    favourites: favouritesReducer, // Toggle Favourites (and set them in LocalStorage)
    cartItems: cartItemsReducer, // Add to Cart (Local Storage) and Remove from Cart
})

// const persistConfig = {
//     key: 'localData',
//     storage
// }

// const localStorageReducer = persistReducer(persistConfig, combinedReducer);

export const store = configureStore({
    reducer: combinedReducer,
    // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)

    // below is my trying of usage of redux-persist , and trying to fix "non-serialized" issue

    // reducer: localStorageReducer,

    // middleware: getDefaultMiddleware({
    //     serializableCheck: {
    //       ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
    //     },
    //   }),
    
    // middleware: getDefaultMiddleware =>
    // getDefaultMiddleware({
    //   serializableCheck: false,
    // }),
    
}) 
