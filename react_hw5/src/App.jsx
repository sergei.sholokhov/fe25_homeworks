import { Routes, Route } from 'react-router-dom';
import Header from "./components/header/Header";
import HomePage from "./components/products/HomePage";
import Cart from './components/cart/Cart';
import Favourites from './components/favorites/Favourites';
import './index.scss';

const App = () => {

    return (
        <div className="app-container">
            <Header />
            <Routes>
                <Route path='/' element={<HomePage />} />
                <Route path='/favourites' element={<Favourites />} />
                <Route path='/cart' element={<Cart />} />
            </Routes>
        </div>
    );
};

export default App;
