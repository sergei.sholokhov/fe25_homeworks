import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
// import ModalImage from './ModalImage'
import { useDispatch } from 'react-redux';
import { addToCartAction, removeFromCartAction } from '../../redux/cartItems.slice/cartItems.slice';
import { closeModalAction } from '../../redux/modal.slice/modal.slice'
import PropTypes from 'prop-types'

const ModalBase = ({
    modalProduct,
    isOpen,
    title,
    image,
    description,
    isSecondModalToggle }) => {

    const dispatch = useDispatch();

    const handleClose = () => {
        dispatch(closeModalAction());
    }

    const handleAddToCart = () => {
        dispatch(addToCartAction(modalProduct));
        handleClose();
        // console.log('addToCart modalProduct', modalProduct);
    }

    const handleDeleteFromCart = () => {
        dispatch(removeFromCartAction(modalProduct));
        handleClose();
        // console.log('removeFromCart', modalProduct);
    }

    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleClose()
        }
    }

    return (
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={handleClose} />
                    {/* {isSecondModalToggle && <ModalImage />} */}
                </ModalHeader>
                <ModalBody>
                    <h2>{title}</h2>
                    <img src={image} alt='image}' style={{ height: '200px' }} />
                    <div>{description}</div>
                </ModalBody>
                <ModalFooter
                    firstText={isSecondModalToggle ? "YES, DELETE" : "Add to Cart"}
                    secondaryText={isSecondModalToggle ? "NO, CANCEL" : ""}
                    firstClick={isSecondModalToggle ?
                        () => handleDeleteFromCart(modalProduct) : () => handleAddToCart(modalProduct)}
                    secondaryClick={handleClose}
                />
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    modalProduct: PropTypes.object,
    title: PropTypes.string,
    description: PropTypes.any,
    handleCancel: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalBase