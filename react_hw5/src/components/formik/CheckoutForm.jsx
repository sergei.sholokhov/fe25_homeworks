import { useDispatch, useSelector } from 'react-redux';
import { resetCartItemsAction } from '../../redux/cartItems.slice/cartItems.slice';
import Select from './SelectRegion';
import Checkbox from './Checkbox';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import './Checkout.scss';

export default function CheckoutForm() {

    const cartItems = useSelector(state => state.cartItems.cartItems);

    if (cartItems.length === 0) {
        // return null; 
        return <h2>Cart is empty</h2>;
    }

    const dispatch = useDispatch();

    const validationSchema = Yup.object().shape({
        firstName: Yup.string().required('First name is required')
            .matches(/^[A-Za-z]+$/, 'First name cannot contain numbers'),
        lastName: Yup.string().required('Last name is required')
            .matches(/^[A-Za-z]+$/, 'Last name cannot contain numbers'),
        email: Yup.string().email('Invalid email').required('Email is required'),
        year: Yup.number().required('Year of birth is required')
            .test(
                'is-old-enough',
                "You're not yet 18 years old",
                function (value) {
                    const currentYear = new Date().getFullYear();
                    return currentYear - value >= 18;
                }
            ),
        phoneNumber: Yup.string().required('Phone number is required')
            .matches(/^[0-9]+$/, 'Must contain only numbers'),
        region: Yup.string().required("Region is required"),
        city: Yup.string().required('City is required')
            .matches(/^[A-Za-z]+$/, 'City cannot contain numbers'),
        postalCode: Yup.string().required('Postal Code is required'),
        street: Yup.string().required('Street address is required'),
        suite: Yup.number().required('Suite number is required'),
        acceptedTerms: Yup.boolean()
            .required("Required")
            .oneOf([true], "You must accept the terms and conditions."),
    });

    const initialValues = {
        firstName: '',
        lastName: '',
        email: '',
        year: '',
        phoneNumber: '',
        region: '',
        city: '',
        postalCode: '',
        street: '',
        suite: '',
    }

    function onSubmit(values) {
        const cartItems = JSON.parse(localStorage.getItem('cartItems'));
        console.log('User info', values);
        console.log('The order:', cartItems);
        localStorage.removeItem('cartItems');
        dispatch(resetCartItemsAction());
        alert("Thank you for making a purchase!");
    }

    return (
        <Formik
            initialValues={initialValues}
            onSubmit={onSubmit}
            validationSchema={validationSchema}
        >
            <div className="form-container">

                <Form>
                    <div className='form-container_content'>
                        <div className='contact'>
                            <h2>Contact Information</h2>

                            <div className="form-element">
                                <label htmlFor='firstName'>First name*</label>
                                <Field type="text" id="firstName" name="firstName" placeholder="Enter your first name" />
                                <ErrorMessage name="firstName" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='lastName'>Last name*</label>
                                <Field type="text" id="lastName" name="lastName" placeholder="Enter your last name" />
                                <ErrorMessage name="lastName" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='email'>Email*</label>
                                <Field type="email" id="email" name="email" placeholder="Enter your email" />
                                <ErrorMessage name="email" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='year'>Birth year*</label>
                                <Field as="select" id="year" name="year" placeholder="Select your birth year">
                                    {
                                        new Array(100)
                                            .fill('')
                                            .map((item, index) => new Date().getFullYear() - index)
                                            .map(year => (
                                                <option value={year} key={year}>
                                                    {year}
                                                </option>
                                            ))
                                    }
                                </Field>
                                <ErrorMessage name="year" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor="phoneNumber">Phone Number*</label>
                                <Field type='text' id='phoneNumber' name='phoneNumber' placeholder="Enter your phone number"></Field>
                                <ErrorMessage name='phoneNumber' component='div' className='error-message'></ErrorMessage>
                            </div>
                        </div>

                        <div className='contact'>
                            <h2>Delivery address</h2>

                            <Select
                                id='region'
                                label="Province or Territory*"
                                name='region'
                            />

                            <div className="form-element">
                                <label htmlFor='city'>City*</label>
                                <Field type="text" id="city" name="city" placeholder="City" />
                                <ErrorMessage name="city" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='postalCode'>Postal Code*</label>
                                <Field type="text" id="postalCode" name="postalCode" placeholder="Postal Code" />
                                <ErrorMessage name="postalCode" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='street'>Street Address*</label>
                                <Field type="text" id="street" name="street" placeholder="Street Address" />
                                <ErrorMessage name="street" component="div" className='error-message' />
                            </div>

                            <div className="form-element">
                                <label htmlFor='suite'>Suite Number*</label>
                                <Field type="text" id="suite" name="suite" placeholder="Suite Number" />
                                <ErrorMessage name="suite" component="div" className='error-message' />
                            </div>
                        </div>
                    </div>

                    <div className='confirm'>
                        <Checkbox name="acceptedTerms">I accept the terms and conditions</Checkbox>
                        <button type="submit" className='checkout-button'>Checkout</button>
                    </div>
                </Form>
            </div>
        </Formik >
    )
}


