import Product from "../products/Product";
import ModalBase from '../modal/ModalBase';
import ProductDescription from "../products/ProductDescription";
import { useSelector } from 'react-redux';

export default function Favorites() {

    const { favourites } = useSelector((state) => state.favourites)
    const isAddModalOpen = useSelector(state => state.modal.isAddModalOpen);
    const modalProduct = useSelector((state) => state.modal.modalProduct);

    const favouritesStar = favourites.map((f) => {
        return { ...f, isFavourite: true }
    })

    return (
        <>
            <div className="favourites">
                <h2>Favourites</h2>
                <div className="product-grid">
                    {favouritesStar.map((product) => (
                        <Product
                            key={product.article}
                            product={product}
                            cross
                        />
                    ))}
                </div>
            </div>

            {isAddModalOpen && (
                <ModalBase
                    modalProduct={modalProduct}
                    isOpen={isAddModalOpen}
                    title={modalProduct.name}
                    image={modalProduct.image}
                    description={<ProductDescription desc={modalProduct} />}
                    isSecondModalToggle={false}
                />
            )}
        </>
    )
}



