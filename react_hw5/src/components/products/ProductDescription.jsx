import React from "react";
import PropTypes from 'prop-types'

const ProductDescription = ({ desc }) => {
    return (
        <>
            {/* <img src={desc.image} alt='image}' style={{ height: '200px' }} /> */}
            <p>color: {desc.color}</p>
            <p>price: {desc.price}</p>
        </>
    )
}

ProductDescription.propTypes = {
    desc: PropTypes.object
}

export default ProductDescription;