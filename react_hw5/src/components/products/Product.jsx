import React from 'react';
import Button from '../button/Button';
import DeleteButton from '../button/DeleteButton';
import { FaStar } from 'react-icons/fa';
import { useDispatch } from 'react-redux';
import { toggleFavouritesAction } from '../../redux/favourites.slice/favourites.slice';
import { addToCartModalAction, deleteFromCartModalAction } from '../../redux/modal.slice/modal.slice';
import PropTypes from 'prop-types'

const Product = ({
    product,
    cross = false,
    add = false
}) => {

    const dispatch = useDispatch();

    const handleToggleFavourites = () => {
        dispatch(toggleFavouritesAction(product));
    };

    const handleAddModal = (product) => {
        dispatch(addToCartModalAction(product));
    }

    const handleDeleteModal = (product) => {
        dispatch(deleteFromCartModalAction(product))
    }

    return (
        <div className="product-item">
            {!cross && <DeleteButton onDelete={() => handleDeleteModal(product)} />}
            <h2>{product.name}</h2>
            <p> Price: {product.price}</p>
            <div className='image-block' >
                <img src={product.image} alt={product.name} />
            </div>
            <p>Color: {product.color}</p>

            {!add && (
                <Button
                    type="button"
                    classNames="button first-button"
                    onClick={() => handleAddModal(product)}
                >
                    Add to Cart
                </Button>
            )}

            <div className="fav-block" onClick={handleToggleFavourites} style={{ cursor: 'pointer' }}>
                <FaStar className="fav-star" style={{ color: product.isFavourite ? 'yellow' : 'gray' }} />
                <p>Add to Favorites</p>
            </div>
        </div>
    )
}

Product.propTypes = {
    product: PropTypes.object,
    cross: PropTypes.bool,
    add: PropTypes.bool
}

export default Product;

