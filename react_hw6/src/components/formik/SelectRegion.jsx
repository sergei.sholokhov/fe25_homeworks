import React from "react";
import { Field, ErrorMessage } from "formik";

const regions = [
    { value: "", label: "Select Province or Territory" },
    { value: "ab", label: "Alberta" },
    { value: "bc", label: "British Columbia" },
    { value: "mn", label: "Manitoba" },
    { value: "nb", label: "New Brunswick" },
    { value: "nl", label: "Newfoundland and Labrador" },
    { value: "ns", label: "Nova Scotia" },
    { value: "on", label: "Ontario" },
    { value: "pei", label: "Prince Edward Island" },
    { value: "sk", label: "Saskatchewan" },
    { value: "qc", label: "Quebec" },
    { value: "nv", label: "Nunavut" },
    { value: "nwt", label: "The Northwest Territories" },
    { value: "yk", label: "The Yukon" }
];

const Select = ({ label, name, id }) => {
    return (
        <div className="form-element">
            <label htmlFor={id}>{label}</label>
            <Field as='select' id={id} name={name}>
                {regions.map(region => (
                    <option key={region.value} value={region.value}>
                        {region.label}
                    </option>
                ))}
            </Field>
            <ErrorMessage name={name} component="div" className="error-message" />
        </div>
    )
}

export default Select;
