import React from 'react';
import PropTypes from 'prop-types'
import './Button.scss';

const Button = ({ type, classNames, onClick, children }) => {
    
    return (
        <button
            type={type || 'button'}
            className={classNames || 'button'}
            onClick={onClick}
            data-testid="button"
        >
            {children}
        </button>
    );
};

Button.propTypes = {
    type: PropTypes.string, 
    classNames: PropTypes.string, 
    onClick: PropTypes.any, 
    children: PropTypes.any
}

export default Button;

