// import Product from './Product';
// import ModalBase from '../modal/ModalBase';
// import ProductDescription from "./ProductDescription";
// import { useDispatch, useSelector } from 'react-redux';
// import { useEffect } from 'react';
// import { fetchProducts } from '../../redux/products.slice/products.slice';
// import { useDisplayMode } from '../../context/DisplayModeContext'; 
// import './ProductList.scss';

// export default function HomePage() {

//     const dispatch = useDispatch();
//     const { products } = useSelector((state) => state.products);

//     useEffect(() => {
//         dispatch(fetchProducts());
//     }, [dispatch]);

//     const isAddModalOpen = useSelector(state => state.modal.isAddModalOpen);
//     const isDeleteModalOpen = useSelector((state) => state.modal.isDeleteModalOpen)
//     const modalProduct = useSelector((state) => state.modal.modalProduct);
//     const favourites = useSelector((state) => state.favourites.favourites)

//     const productsWithFavorites = products.map((p) => {
//         const favouriteProduct = favourites.find((f) => {
//             return f.article === p.article
//         })
//         return {
//             ...p, isFavourite: !!favouriteProduct
//         }
//     })

//     return (
//         <>
//             <h2>Our Products available in store</h2>
//             <div className="product-grid">
//                 {productsWithFavorites.map((product) => (
//                     <Product
//                         product={product}
//                         key={product.article}
//                     />
//                 ))}
//             </div>

//             {isAddModalOpen && (
//                 <ModalBase
//                     modalProduct={modalProduct}
//                     isOpen={isAddModalOpen}
//                     title={modalProduct.name}
//                     image={modalProduct.image}
//                     description={<ProductDescription desc={modalProduct} />}
//                     isSecondModalToggle={false}
//                 />
//             )}

//             {isDeleteModalOpen && (
//                 <ModalBase
//                     modalProduct={modalProduct}
//                     isOpen={isDeleteModalOpen}
//                     title="Delete Confirmation"
//                     image={modalProduct.image}
//                     description={`Are you sure you want to delete ${modalProduct.name} from your cart?`}
//                     isSecondModalToggle={true}
//                 />
//             )}

//         </>
//     );
// }

import React, { useEffect } from 'react';
import Product from './Product';
import ModalBase from '../modal/ModalBase';
import ProductDescription from "./ProductDescription";
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../../redux/products.slice/products.slice';
import { useDisplayMode } from '../../context/DisplayModeContext'; 
import './ProductList.scss';

export default function HomePage() {
    const dispatch = useDispatch();
    const { products } = useSelector((state) => state.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    const { displayMode, toggleDisplayMode } = useDisplayMode();

    const isAddModalOpen = useSelector(state => state.modal.isAddModalOpen);
    const isDeleteModalOpen = useSelector((state) => state.modal.isDeleteModalOpen)
    const modalProduct = useSelector((state) => state.modal.modalProduct);
    const favourites = useSelector((state) => state.favourites.favourites)

    const productsWithFavorites = products.map((p) => {
        const favouriteProduct = favourites.find((f) => {
            return f.article === p.article
        })
        return {
            ...p, isFavourite: !!favouriteProduct
        }
    })

    return (
        <>
            <div className="headline-button">
            <h2>Our Products available in store</h2>
                <button onClick={toggleDisplayMode} className='toggle-button'>
                    {displayMode === 'cards' ? 'Switch to Table View' : 'Switch to Card View'}
                </button>
            </div>
            <div className={displayMode === 'cards' ? 'product-grid' : 'product-table'}>
                {productsWithFavorites.map((product) => (
                    <Product
                        product={product}
                        key={product.article}
                    />
                ))}
            </div>

            {isAddModalOpen && (
                <ModalBase
                    modalProduct={modalProduct}
                    isOpen={isAddModalOpen}
                    title={modalProduct.name}
                    image={modalProduct.image}
                    description={<ProductDescription desc={modalProduct} />}
                    isSecondModalToggle={false}
                />
            )}

            {isDeleteModalOpen && (
                <ModalBase
                    modalProduct={modalProduct}
                    isOpen={isDeleteModalOpen}
                    title="Delete Confirmation"
                    image={modalProduct.image}
                    description={`Are you sure you want to delete ${modalProduct.name} from your cart?`}
                    isSecondModalToggle={true}
                />
            )}

        </>
    );
}


