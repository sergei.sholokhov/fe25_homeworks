import cartItemsReducer, { addToCartAction, removeFromCartAction, resetCartItemsAction } from '../redux/cartItems.slice/cartItems.slice';

describe('cartItemsReducer', () => {
  test('should return the initial state', () => {
    const initialState = {
      cartItems: [],
      cartCount: 0
    };
    expect(cartItemsReducer(undefined, {})).toEqual(initialState);
  });

  test('should handle addToCart action', () => {
    const payload = { article: 1, name: 'Product 1' };
    const prevState = {
      cartItems: [],
      cartCount: 0
    };
    const action = addToCartAction(payload);
    const expectedState = {
      cartItems: [{ article: 1, name: 'Product 1' }],
      cartCount: 1
    };
    expect(cartItemsReducer(prevState, action)).toEqual(expectedState);
  });

  test('should handle removeFromCart action', () => {
    const payload = { article: 1, name: 'Product 1' };
    const prevState = {
      cartItems: [{ article: 1, name: 'Product 1' }],
      cartCount: 1
    };
    const action = removeFromCartAction(payload);
    const expectedState = {
      cartItems: [],
      cartCount: 0
    };
    expect(cartItemsReducer(prevState, action)).toEqual(expectedState);
  });

  test('should handle resetCartItems action', () => {
    const prevState = {
      cartItems: [{ article: 1, name: 'Product 1' }],
      cartCount: 1
    };
    const action = resetCartItemsAction();
    const expectedState = {
      cartItems: [],
      cartCount: 0
    };
    expect(cartItemsReducer(prevState, action)).toEqual(expectedState);
  });
});
