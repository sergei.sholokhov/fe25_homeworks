import favouritesReducer, { toggleFavouritesAction } from '../redux/favourites.slice/favourites.slice';

describe('favouritesReducer', () => {
  test('should return the initial state', () => {
    const initialState = {
      favourites: [],
      favouritesCount: 0
    };
    expect(favouritesReducer(undefined, {})).toEqual(initialState);
  });

  test('should handle toggleFavourites action when item is added', () => {
    const payload = { article: 1, name: 'Product 1' };
    const prevState = {
      favourites: [],
      favouritesCount: 0
    };
    const action = toggleFavouritesAction(payload);
    const expectedState = {
      favourites: [{ article: 1, name: 'Product 1' }],
      favouritesCount: 1
    };
    expect(favouritesReducer(prevState, action)).toEqual(expectedState);
  });

  test('should handle toggleFavourites action when item is removed', () => {
    const payload = { article: 1, name: 'Product 1' };
    const prevState = {
      favourites: [{ article: 1, name: 'Product 1' }],
      favouritesCount: 1
    };
    const action = toggleFavouritesAction(payload);
    const expectedState = {
      favourites: [],
      favouritesCount: 0
    };
    expect(favouritesReducer(prevState, action)).toEqual(expectedState);
  });
});
