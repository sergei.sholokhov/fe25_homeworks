import modalReducer,
{
    addToCartModalAction,
    deleteFromCartModalAction,
    closeModalAction
} from '../redux/modal.slice/modal.slice';

describe('modalReducer', () => {
    test('should return the initial state', () => {
        const initialState = {
            isAddModalOpen: false,
            isDeleteModalOpen: false,
            modalProduct: null
        };
        expect(modalReducer(undefined, {})).toEqual(initialState);
    });

    test('should handle addToCartModal action', () => {
        const payload = { id: 1, name: 'Product 1' };
        const action = addToCartModalAction(payload);
        const expectedState = {
            isAddModalOpen: true,
            isDeleteModalOpen: false,
            modalProduct: { id: 1, name: 'Product 1' }
        };
        expect(modalReducer(undefined, action)).toEqual(expectedState);
    });

    test('should handle deleteFromCartModal action', () => {
        const payload = { id: 1, name: 'Product 1' };
        const action = deleteFromCartModalAction(payload);
        const expectedState = {
            isAddModalOpen: false,
            isDeleteModalOpen: true,
            modalProduct: { id: 1, name: 'Product 1' }
        };
        expect(modalReducer(undefined, action)).toEqual(expectedState);
    });

    test('should handle closeModal action', () => {
        const prevState = {
            isAddModalOpen: true,
            isDeleteModalOpen: true,
            modalProduct: { id: 1, name: 'Product 1' }
        };
        const action = closeModalAction();
        const expectedState = {
            isAddModalOpen: false,
            isDeleteModalOpen: false,
            modalProduct: null
        };
        expect(modalReducer(prevState, action)).toEqual(expectedState);
    });
});
