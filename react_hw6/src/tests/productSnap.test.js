import React from 'react';
import { render } from '@testing-library/react';
import Product from '../components/products/Product';

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: jest.fn(),
}));

const mockProduct = {
  id: 1,
  name: 'Test Product',
  price: 20,
  image: 'test-image.jpg',
  color: 'red',
  isFavourite: false,
};

describe('<Product />', () => {
  test('renders product with basic details', () => {
    const { asFragment } = render(<Product product={mockProduct} />);
    expect(asFragment()).toMatchSnapshot();
  });

  test('renders product with delete button', () => {
    const { asFragment } = render(<Product product={mockProduct} cross={true} />);
    expect(asFragment()).toMatchSnapshot();
  });

  test('renders product with add button', () => {
    const { asFragment } = render(<Product product={mockProduct} add={true} />);
    expect(asFragment()).toMatchSnapshot();
  });

  test('renders product with favorite star', () => {
    const { asFragment } = render(<Product product={mockProduct} />);
    expect(asFragment()).toMatchSnapshot();
  });

  test('renders product with favorite star and added to favorites', () => {
    const productWithFavorite = { ...mockProduct, isFavourite: true };
    const { asFragment } = render(<Product product={productWithFavorite} />);
    expect(asFragment()).toMatchSnapshot();
  });
});
