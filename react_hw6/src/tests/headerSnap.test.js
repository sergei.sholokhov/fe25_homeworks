import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { useSelector } from 'react-redux';
import Header from '../components/header/Header';
import { BrowserRouter } from 'react-router-dom';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

describe('<Header />', () => {
  const mockedFavouritesCount = 3;
  const mockedCartCount = 5;

  beforeEach(() => {
    useSelector.mockReturnValueOnce(mockedFavouritesCount); 
    useSelector.mockReturnValueOnce(mockedCartCount); 
  });

  test('renders correctly', () => {
    const { asFragment } = render(
      <BrowserRouter>
        <Header />
      </BrowserRouter>);
    expect(asFragment()).toMatchSnapshot();
  });
});
