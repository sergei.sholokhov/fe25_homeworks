import React from 'react';
import { render, fireEvent, cleanup } from "@testing-library/react"
import "@testing-library/jest-dom";
import Button from "../components/button/Button";
import DeleteButton from '../components/button/DeleteButton';

describe("Buttons", () => {
  let mockHandleClick;
  beforeEach(() => {
    mockHandleClick = jest.fn();
  })

  afterEach(() => {
    cleanup();
  })

  test('should fire click event on button', () => {
    const { getByTestId } = render(
      <Button onClick={mockHandleClick} />
    );
    const button = getByTestId('button');
    // console.log('button', button);
    fireEvent.click(button);
    expect(mockHandleClick).toHaveBeenCalledTimes(1);
  })

  test('should fire click event on Delete button', () => {
    const { getByTestId } = render(
      <DeleteButton onDelete={mockHandleClick} />
    );
    const button = getByTestId('button');
    fireEvent.click(button);
    expect(mockHandleClick).toHaveBeenCalledTimes(1);
  });
  
})

