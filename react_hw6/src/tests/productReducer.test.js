import productsReducer, { fetchProducts } from '../redux/products.slice/products.slice';

describe('productsReducer', () => {
  test('should return the initial state', () => {
    const initialState = {
      products: [],
      status: null
    };
    expect(productsReducer(undefined, {})).toEqual(initialState);
  });

  test('should handle fetchProducts.pending action', () => {
    const action = { type: fetchProducts.pending.type };
    const prevState = {
      products: [],
      status: null
    };
    const expectedState = {
      products: [],
      status: 'pending'
    };
    expect(productsReducer(prevState, action)).toEqual(expectedState);
  });

  test('should handle fetchProducts.rejected action', () => {
    const action = { type: fetchProducts.rejected.type };
    const prevState = {
      products: [],
      status: null
    };
    const expectedState = {
      products: [],
      status: 'error'
    };
    expect(productsReducer(prevState, action)).toEqual(expectedState);
  });

  test('should handle fetchProducts.fulfilled action', () => {
    const action = { 
      type: fetchProducts.fulfilled.type,
      payload: [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }]
    };
    const prevState = {
      products: [],
      status: null
    };
    const expectedState = {
      products: [{ id: 1, name: 'Product 1' }, { id: 2, name: 'Product 2' }],
      status: 'done'
    };
    expect(productsReducer(prevState, action)).toEqual(expectedState);
  });
});
