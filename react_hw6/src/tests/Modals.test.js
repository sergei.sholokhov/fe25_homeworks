import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom';
import ModalBase from '../components/modal/ModalBase';
import { useDispatch } from 'react-redux';
import { closeModalAction } from '../redux/modal.slice/modal.slice';
import { addToCartAction, removeFromCartAction } from '../redux/cartItems.slice/cartItems.slice'

jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
}));

describe('<ModalBase/>', () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    useDispatch.mockReturnValue(mockDispatch);
  });

  test('renders modal with title, description, and image', () => {
    const modalProduct = {
      id: 1,
      title: 'Product Title',
      description: 'Product Description',
      image: 'image-url',
    };
    const { getByText, getByAltText } = render(
      <ModalBase
        modalProduct={modalProduct}
        isOpen={true}
        title={modalProduct.title}
        description={modalProduct.description}
        image={modalProduct.image}
      />
    );
    expect(getByText('Product Title')).toBeInTheDocument();
    expect(getByText('Product Description')).toBeInTheDocument();
    expect(getByAltText('image')).toBeInTheDocument();

    expect(getByAltText('image')).toHaveAttribute('src', 'image-url');
  });

  test('closes modal when close button is clicked', () => {
    const { getByTestId } = render(
      <ModalBase isOpen={true} />
    );
    const closeButton = getByTestId('button');
    fireEvent.click(closeButton);
    expect(mockDispatch).toHaveBeenCalledWith(closeModalAction());
  });

  test('adds product to cart when "Add to Cart" button is clicked', () => {
    const modalProduct = {
      id: 1,
      title: 'Product Title',
      description: 'Product Description',
      image: 'image-url',
    };
    const { getByText } = render(
      <ModalBase
        modalProduct={modalProduct}
        isOpen={true}
        title={modalProduct.title}
        description={modalProduct.description}
      />
    );
    const addToCartButton = getByText('Add to Cart');
    fireEvent.click(addToCartButton);
    expect(mockDispatch).toHaveBeenCalledWith(addToCartAction(modalProduct));
  });

  test('deletes product from cart when "YES, DELETE" button is clicked', () => {
    const modalProduct = {
      id: 1,
      title: 'Product Title',
      description: 'Product Description',
      image: 'image-url',
    };
    const { getByText } = render(
      <ModalBase
        modalProduct={modalProduct}
        isOpen={true}
        title={modalProduct.title}
        description={modalProduct.description}
        isSecondModalToggle={true}
      />
    );
    const deleteButton = getByText('YES, DELETE');
    fireEvent.click(deleteButton);
    expect(mockDispatch).toHaveBeenCalledWith(removeFromCartAction(modalProduct));
  });
});
