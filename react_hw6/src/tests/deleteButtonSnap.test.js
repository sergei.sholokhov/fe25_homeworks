import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom'; 

import DeleteButton from '../components/button/DeleteButton';

describe('<DeleteButton />', () => {
  test('renders correctly', () => {
    const { asFragment } = render(<DeleteButton />);
    expect(asFragment()).toMatchSnapshot();
  });
});
