import { configureStore, combineReducers} from '@reduxjs/toolkit';
// import { getDefaultMiddleware } from '@reduxjs/toolkit';
import productsReducer from './products.slice/products.slice';
import favouritesReducer from './favourites.slice/favourites.slice';
import cartItemsReducer from './cartItems.slice/cartItems.slice';
import modalReducer from './modal.slice/modal.slice';
import logger from 'redux-logger';


const combinedReducer = combineReducers({
    products: productsReducer, 
    modal: modalReducer, 
    favourites: favouritesReducer, 
    cartItems: cartItemsReducer, 
})

export const store = configureStore({
    reducer: combinedReducer,
    // middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
}) 
