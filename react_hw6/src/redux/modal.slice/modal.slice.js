import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isAddModalOpen: false,
  isDeleteModalOpen: false,
  modalProduct: null,
};

const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    addToCartModal: (state, action) => {
      state.isAddModalOpen = true;
      state.modalProduct = action.payload;
    },
    deleteFromCartModal: (state, action) => {
      state.isDeleteModalOpen = true;
      state.modalProduct = action.payload;
    },
    closeModal: (state) => {
      state.isAddModalOpen = false;
      state.isDeleteModalOpen = false;
      state.modalProduct = null;
    },
  },
});

export const {
  addToCartModal: addToCartModalAction,
  deleteFromCartModal: deleteFromCartModalAction,
  closeModal: closeModalAction
} = modalSlice.actions;

export default modalSlice.reducer;
