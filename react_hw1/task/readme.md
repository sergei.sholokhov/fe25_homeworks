## Завдання

Створити новий React додаток з двома модальними вікнами.

#### Технічні вимоги:
- Створити програму за допомогою `Create-React-app` або` vite`.
- Створити на головній сторінці 2 кнопки з текстом `Open first modal` та `Open second modal`.
- По кліку на кожну з кнопок має відкриватись відповідне модальне вікно.
- Створіть компонент `Button`, яка повинна мати наступний `props`:
  - [type, classNames, onClick, children]
  - Функція при натисканні (властивість `onclick`)
  - Компонент повинен виявитися універсальним.
- Створіть компонент `Modal`, який слід представляти у вигляді конструктора, повинен складатися з компонентів:
  - ModalWrapper - Для того, щоб зробити тінь підкладки, повинен мати `props` children.
  - modalheader - який повинен мати `props` children.
  - Modalfooter - в якому можуть бути кнопки 1 або 2 і повинні мати такі `props`:
    - firstText, secondaryText, firstClick, secondaryClick
    - Якщо є `props` для першого текст, то покажіть кнопку 1, якщо для другого, то покажіть кнопку 2
  - ModalClose - хрест для закриття модального, повинен мати `props` onclick.
  - modalbody - для вмісту, який повинен приймати `props` children.
  - Modal - компонент врапер модалки, повинен мати `props` children.
- При відкритому модальному вікні частина сторінки, що залишилася, повинна бути затемнена за допомогою темного напівпрозорого фону.
- Модальне вікно має закриватися при натисканні області зовні його контенту.
- Стилізувати кнопки та модальні вікна за допомогою SCSS
- Зберіть 2 варіанти для модалки `ModalImage` & `ModalText` на основі компонентів `Modal`
- Дизайн модального вікна дається в [Figma](https://www.figma.com/file/9DKg7QirJXqRJXd9PhpwUt/Euphoria-React_danIT_popup?type=design&node-id=20%3A86&mode=design&t=NCYLmK2lzg6ilDHf-1) файлі.
- Усі компоненти повинні бути створені як функціональні компоненти

#### Необов'язкове завдання підвищеної складності
- Замість SCSS стилізувати компоненти за допомогою JSS або Styled Components
