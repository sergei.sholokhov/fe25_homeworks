import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import ModalImage from './ModalImage'

const ModalBase = ({ title, desc, handleClose, isOpen, handleOk, handleCancel, isSecondModalToggle }) => {

    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleClose()
        }
    }

    return (
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={handleClose} />
                    {isSecondModalToggle && <ModalImage />}
                </ModalHeader>
                <ModalBody>
                    <h2>{title}</h2>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter
                    firstText={isSecondModalToggle ? "NO, CANCEL" : "ADD TO FAVORITE"}
                    secondaryText={isSecondModalToggle ? "YES, DELETE" : ""}
                    firstClick={handleOk}
                    secondaryClick={handleCancel}
                />
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    desc: PropTypes.string,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalBase