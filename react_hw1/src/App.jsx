import React, { useState } from 'react';

import Button from './components/button/Button';
import ModalBase from './components/modal/ModalBase';

const App = () => {

  const [isFirstModalToggle, setFirstModalToggle] = useState(false);
  const toggleFirstModal = () => setFirstModalToggle(!isFirstModalToggle)

  const [isSecondModalToggle, setSecondModalToggle] = useState(false);
  const toggleSecondModal = () => setSecondModalToggle(!isSecondModalToggle);

  const handleFirstButton = () => alert('test First button');
  const handleSecondButton = () => alert('test Second button');

  return (
    <div className="App">
      <Button
        type="button"
        classNames="button first-button"
        onClick={toggleFirstModal}
      >
        Open first modal
      </Button>

      <Button
        type="button"
        classNames="button second-button"
        onClick={toggleSecondModal}
      >
        Open second modal
      </Button>

      {isFirstModalToggle && (
        <ModalBase
          isOpen={isFirstModalToggle}
          title='Add Product "NAME"'
          desc='Description of your product'
          handleClose={() => { setFirstModalToggle(false) }}
          handleOk={handleFirstButton}
          handleCancel={handleSecondButton}
          isSecondModalToggle={false}
        />
      )}

      {isSecondModalToggle && (
        <ModalBase
          isOpen={isSecondModalToggle}
          title='Product Delete!'
          desc='By clicking the "Yes, Delete" button, PRODUCT NAME will be deleted.'
          handleClose={() => { setSecondModalToggle(false) }}
          handleOk={handleFirstButton}
          handleCancel={handleSecondButton}
          isSecondModalToggle={true}
        />
      )} 

    </div>
  );
};

export default App;

