import { useState, useEffect } from "react";

import ProductList from "./components/products/ProductList";
import Header from "./components/header/Header";
import './index.scss'

const App = () => {
  const [isReady, setIsReady] = useState(false)
  const [favoritesCount, setFavoritesCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  const [favorites, setFavorites] = useState([]);

  useEffect(() => {
    const favoritesParsed = JSON.parse(localStorage.getItem("favorites")) || [];
    const cartItemsParsed = JSON.parse(localStorage.getItem("cartItems")) || [];
    setFavoritesCount(favoritesParsed.length);
    setCartCount(cartItemsParsed.length);
    setFavorites(favoritesParsed);
    setIsReady(true)
  }, [favoritesCount, cartCount]);

  return (
    <div className="app-container">
      <Header cartCount={cartCount} favoritesCount={favoritesCount} />
      {isReady ? (
          <ProductList
            setFavoritesCount={setFavoritesCount}
            setCartCount={setCartCount}
            favorites={favorites}
          />
      ) : null}
    </div>
  );
};

export default App;
