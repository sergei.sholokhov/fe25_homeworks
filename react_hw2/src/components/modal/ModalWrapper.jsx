import PropTypes from 'prop-types'
import './Modal.scss'

const ModalWrapper = ({children, isOpen, handleOutside}) =>{
    return(
        <>
        {isOpen && (<div className="modal-wrapper" onClick={(event)=>handleOutside(event)}>{children}</div>)}
        </>
    )
}

ModalWrapper.propTypes = {
    children: PropTypes.any,
    isOpen: PropTypes.bool, 
    handleOutside: PropTypes.func
}
export default ModalWrapper