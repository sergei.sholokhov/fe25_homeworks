import React, { useState, useEffect, useCallback } from "react";
import PropTypes from 'prop-types';
import Product from "./Product";
import "./ProductList.scss";
import ModalBase from "./../modal/ModalBase";
import ProductDescription from "./ProductDescription";

const ProductList = ({ setFavoritesCount, setCartCount, favorites }) => {
    const [products, setProducts] = useState([]);
    const [modalProductArticle, setModalProductArticle] = useState(false);

    const modalProduct = products.find(
        (p) => modalProductArticle && p.article === modalProductArticle
    );

    useEffect(() => {
        fetch("/products.json")
            .then((response) => response.json())
            .then((data) => setProducts(data.products))
            .catch((error) => console.error("Error fetching products:", error));
    }, []);

    const productsWithFavorites = products.map((d) => {
        const favouriteProduct = favorites.find((f) => {
            return f.article === d.article
        })
        return {
            ...d, isFavorite: !!favouriteProduct
        }
    })

    const toggleFavorite = useCallback((product) => { 
        const updatedProducts = products.map((p) => {
            if (p.article === product.article) {
                return { ...p, isFavorite: !p.isFavorite };
            }
            return p;
        });
        setProducts(updatedProducts);

        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        if (!product.isFavorite) {
            favorites.push({
                article: product.article,
                name: product.name,
                price: product.price,
            });
            localStorage.setItem("favorites", JSON.stringify(favorites));
        } else {
            const index = favorites.findIndex(
                (favorite) => favorite.article === product.article
            );
            favorites.splice(index, 1);
            localStorage.setItem("favorites", JSON.stringify(favorites));
        }

        setFavoritesCount(favorites.length);
    }, [products, setFavoritesCount]); //eslint plugin

    const handleAddToCart = () => {
        const cartItemsInStorage = JSON.parse(localStorage.getItem("cartItems")) || [];
        const existingProductIndex = cartItemsInStorage.findIndex(item => item.article === modalProduct.article);

        if (existingProductIndex !== -1) {
            alert(`You already have ${modalProduct.name} item in your cart`);
        }else{

            cartItemsInStorage.push({
                article: modalProduct.article,
                name: modalProduct.name,
                price: modalProduct.price,
            });
        }
        localStorage.setItem("cartItems", JSON.stringify(cartItemsInStorage));

        setCartCount(cartItemsInStorage.length);
    };

    return (
        <>
            <div className="product-grid">
                {productsWithFavorites.map((product) => (
                    <Product
                        key={product.article}
                        product={product}
                        onModal={() => setModalProductArticle(product.article)}
                        toggleFavorite={toggleFavorite}
                        
                    />
                ))}
            </div>

            {modalProduct && (
                <ModalBase
                    isOpen={!!modalProductArticle}
                    title={modalProduct.name}
                    description={<ProductDescription desc={modalProduct} />}
                    handleClose={() => {
                        setModalProductArticle(false);
                    }}
                    handleOk={handleAddToCart}
                    isSecondModalToggle={false}
                />
            )}
        </>
    );
};

ProductList.propTypes = {
    setFavoritesCount: PropTypes.func, 
    setCartCount: PropTypes.func,
    favorites: PropTypes.array
}

export default ProductList;
