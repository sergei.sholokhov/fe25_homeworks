import React, {useState, useEffect} from 'react';
import { FaStar } from 'react-icons/fa';
import PropTypes from 'prop-types'

import Button from './../button/Button';

const Product = ({ product, onModal, toggleFavorite}) => {

    return (
        <div className="product-item">
            <h2>{product.name}</h2>
            <p>Price: {product.price}</p>
            <div className='image-block'>
                <img src={product.image} alt={product.name} />
            </div>
            <p>Color: {product.color}</p>

            <Button
                type="button"
                classNames="button first-button"
                onClick={onModal}
            >
                Add to Cart
            </Button>

            <div className="fav-block" onClick={() => toggleFavorite(product)} style={{ cursor: 'pointer' }}>
                <FaStar className="fav-star" style={{ color: product.isFavorite ? 'yellow' : 'gray' }} />
                <p>Add to Favorites</p>
            </div>

        </div>
    )
}

Product.propTypes = {
    product: PropTypes.object,
    onModal: PropTypes.func, 
    toggleFavorite: PropTypes.func, 
}

export default Product;


