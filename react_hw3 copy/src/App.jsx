import { useState, useEffect } from "react";
import { Routes, Route} from 'react-router-dom'

import Header from "./components/header/Header";
import ProductList from "./components/products/ProductList";
import Cart from './components/cart/Cart';
import Favorites from './components/favorites/Favorites';

import './index.scss'

const App = () => {
    const [favoritesCount, setFavoritesCount] = useState(0);
    const [cartCount, setCartCount] = useState(0);

    useEffect(() => {
        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        const cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
        setFavoritesCount(favorites.length);
        setCartCount(cartItems.length);
    }, [favoritesCount, cartCount]);

    return (
        <div className="app-container">
            <Header
                cartCount={cartCount}
                favoritesCount={favoritesCount}
            />
            <Routes>
                <Route path='/'
                    element={<ProductList
                        setFavoritesCount={setFavoritesCount}
                        setCartCount={setCartCount}
                    />}
                />
                <Route path='/cart' element={<Cart />}></Route>
                <Route path='/favorites' element={<Favorites />}></Route>
            </Routes>
        </div>
    );
};

export default App;
