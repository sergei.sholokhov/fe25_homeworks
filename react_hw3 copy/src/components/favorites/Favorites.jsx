import React, { useState, useEffect } from 'react';
import Product from './../products/Product';
import './../products/ProductList.scss';

const Favorites = () => {
    const [favoriteItems, setFavoriteItems] = useState([]);

    useEffect(() => {
        const storedFavoriteItems = JSON.parse(localStorage.getItem('favorites')) || [];
        setFavoriteItems(storedFavoriteItems);
    }, []);

    return (
        <div className="favorites">
            <h2>Favorites</h2>
            <div className="product-grid">
                {favoriteItems.map((product) => (
                    <Product
                        key={product.article}
                        product={product}
                        isFavorite={true}
                        cross
                    />
                ))}
            </div>
        </div>
    );
};

export default Favorites;

