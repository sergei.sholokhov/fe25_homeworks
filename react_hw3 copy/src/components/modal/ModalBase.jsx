import PropTypes from 'prop-types'
import ModalWrapper from "./ModalWrapper"
import Modal from "./Modal"
import ModalHeader from "./ModalHeader"
import ModalBody from "./ModalBody"
import ModalFooter from "./ModalFooter"
import ModalClose from "./ModalClose"
import ModalImage from './ModalImage'

const ModalBase = ({ title, description, handleClose, isOpen, handleOk, handleCancel, isSecondModalToggle }) => {

    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleClose()
        }
    }

    const handleConfirm = () => {
        handleOk(); 
        handleClose(); 
    }

    return (
        <ModalWrapper isOpen={isOpen} handleOutside={handleOutside}>
            <Modal>
                <ModalHeader>
                    <ModalClose onClick={handleClose} />
                    {isSecondModalToggle && <ModalImage />}
                </ModalHeader>
                <ModalBody>
                    <h2>{title}</h2>
                    <div>{description}</div>
                </ModalBody>
                <ModalFooter
                    firstText={isSecondModalToggle ? "YES, DELETE" : "Add to Cart"}
                    secondaryText={isSecondModalToggle ? "NO, CANCEL" : ""}
                    firstClick={handleConfirm}
                    secondaryClick={handleCancel}
                />
            </Modal>
        </ModalWrapper>
    )
}

ModalBase.propTypes = {
    title: PropTypes.string,
    description: PropTypes.any,
    handleOk: PropTypes.func,
    handleCancel: PropTypes.func,
    handleClose: PropTypes.func,
    isOpen: PropTypes.bool
}

export default ModalBase