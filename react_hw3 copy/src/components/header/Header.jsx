import { FaStar, FaShoppingCart } from 'react-icons/fa';
import PropTypes from 'prop-types'
import HeaderNav from "./HeaderNav";
import './Header.scss';

const Header = ({ favoritesCount, cartCount }) => {

    return (
        <div className="header-flex">
            <h1>Welcome to Magic Music Store</h1>
            <HeaderNav/>
            <div className="header-icons">
                <div className="icon-with-count">
                    <FaStar className="header-icon" />
                    <span className="item-count">{favoritesCount}</span>
                </div>
                <div className="icon-with-count">
                    <FaShoppingCart className="header-icon" />
                    <span className="item-count">{cartCount}</span>
                </div>
            </div>
        </div>
    );
};

Header.propTypes = {
    favoritesCount: PropTypes.number, 
    cartCount: PropTypes.number, 
}

export default Header;

