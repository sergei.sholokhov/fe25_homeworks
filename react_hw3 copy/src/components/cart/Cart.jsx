import React, { useState, useEffect } from 'react';
import Product from './../products/Product';
import './../products/ProductList.scss';

const Cart = () => { //how to prop handleDeleteFromCart from ProductList here?
    const [cartItems, setCartItems] = useState([]);

    useEffect(() => {
        const storedCartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
        setCartItems(storedCartItems);
    }, []);

    const removeCartItem = (article) => {
        const updatedCartItems = cartItems.filter(item => item.article !== article);
        setCartItems(updatedCartItems);
        localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
    };

    console.log(cartItems);

    const handleDeleteFromCart = (article) => {
        const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
        const updatedCartItems = cartItems.filter(item => item.article !== article);
        localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
        console.log('click');
    };

    return (
        <div className="cart">
            <h2>Shopping Cart</h2>
            <div className="product-grid">
                {cartItems.map((product) => (
                    <Product
                        key={product.article}
                        product={product}
                        onRemove={() => removeCartItem(product.article)}
                        isCart={true}
                        handleDelete={() => handleDeleteFromCart(product.article)}

                    />
                ))}
            </div>
        </div>
    );
};

export default Cart;
