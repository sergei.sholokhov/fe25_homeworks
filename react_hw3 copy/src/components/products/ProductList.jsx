import React, { useState, useEffect, useCallback } from "react";
import PropTypes from 'prop-types'
import Product from "./Product";
import ModalBase from "./../modal/ModalBase";
import ProductDescription from "./ProductDescription";

import "./ProductList.scss";

const ProductList = ({ setFavoritesCount, setCartCount }) => {
    const [products, setProducts] = useState([]);
    const [modalProductArticle, setModalProductArticle] = useState(false);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [deleteArticle, setDeleteArticle] = useState(null);


    useEffect(() => {
        fetch("/products.json")
            .then((response) => response.json())
            .then((data) => setProducts(data.products))
            .catch((error) => console.error("Error fetching products:", error));

    }, []);

    const toggleFavorite = useCallback((product) => {
        const updatedProducts = products.map((p) => {
            if (p.article === product.article) {
                return { ...p, isFavorite: !p.isFavorite };
            }
            return p;
        });
        setProducts(updatedProducts);

        const favorites = JSON.parse(localStorage.getItem("favorites")) || [];
        if (!product.isFavorite) {
            favorites.push({
                article: product.article,
                name: product.name,
                price: product.price,
                image: product.image,
                color: product.color

            });
            localStorage.setItem("favorites", JSON.stringify(favorites));
        } else {
            const index = favorites.findIndex(
                (favorite) => favorite.article === product.article
            );
            favorites.splice(index, 1);
            localStorage.setItem("favorites", JSON.stringify(favorites));
        }

        setFavoritesCount(favorites.length);
    }, [products, setFavoritesCount]);

    const modalProduct = products.find(
        (p) => modalProductArticle && p.article === modalProductArticle
    );

    const handleAddToCart = () => {
        const cartItems = JSON.parse(localStorage.getItem("cartItems")) || [];
        cartItems.push({
            article: modalProduct.article,
            name: modalProduct.name,
            price: modalProduct.price,
            image: modalProduct.image,
            color: modalProduct.color
        });
        localStorage.setItem("cartItems", JSON.stringify(cartItems));

        setCartCount(cartItems.length);
    };

    const handleDeleteFromCart = (article) => {
        setDeleteModalOpen(true);
        setDeleteArticle(article);
    };

    const handleDeleteConfirmation = () => {
        const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
        const updatedCartItems = cartItems.filter(item => item.article !== deleteArticle);
        localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
        setDeleteModalOpen(false);
    };

    return (
        <>
            <div className="product-grid">
                {products.map((product) => (
                    <Product
                        key={product.article}
                        product={product}
                        onModal={() => setModalProductArticle(product.article)}
                        toggleFavorite={toggleFavorite}
                        isFavorite={product.isFavorite}
                        handleDelete={() => handleDeleteFromCart(product.article)}
                        openDeleteModal={() => handleDeleteFromCart(product.article)}
                    />
                ))}
            </div>

            {modalProduct && (
                <ModalBase
                    isOpen={!!modalProductArticle}
                    title={modalProduct.name}
                    description={<ProductDescription desc={modalProduct} />}
                    handleClose={() => {
                        setModalProductArticle(false);
                    }}
                    handleOk={handleAddToCart}
                    isSecondModalToggle={false}
                />
            )}

            {deleteModalOpen && (
                <ModalBase
                    isOpen={deleteModalOpen}
                    title="Confirmation"
                    description={`Are you sure you want to delete this item from your cart?`}
                    handleClose={() => setDeleteModalOpen(false)}
                    handleOk={handleDeleteConfirmation}
                    handleCancel={() => setDeleteModalOpen(false)}
                    isSecondModalToggle={true}
                />
            )}
        </>
    );
};

ProductList.propTypes = {
    setFavoritesCount: PropTypes.func, 
    setCartCount: PropTypes.func
}

export default ProductList;

