import Product from "../products/Product";
import ModalBase from '../modal/ModalBase';
import { useSelector } from "react-redux";

export default function Cart() {

    const { cartItems } = useSelector((state) => state.cartItems)
    const { favourites } = useSelector((state) => state.favourites)
    const isDeleteModalOpen = useSelector((state) => state.modal.isDeleteModalOpen)
    const modalProduct = useSelector((state) => state.modal.modalProduct);

    const cartWithFavorites = cartItems.map((c) => {
        const cardFavourites = favourites.find((f) => {
            return f.article === c.article
        })
        return {
            ...c, isFavourite: !!cardFavourites
        }
    })

    return (
        <>
            <div className="cart">
                <h2>Cart</h2>
                <div className="product-grid">
                    {cartWithFavorites.map((product) => (
                        <Product
                            key={product.article}
                            product={product}
                            add
                        />
                    ))}
                </div>
            </div>

            {isDeleteModalOpen && (
                <ModalBase
                    modalProduct={modalProduct}
                    isOpen={isDeleteModalOpen}
                    title="Delete Confirmation"
                    image={modalProduct.image}
                    description={`Are you sure you want to delete ${modalProduct.name} from your cart?`}
                    isSecondModalToggle={true}
                />
            )}

        </>
    )
}


