import PropTypes from 'prop-types'
import Button from "../button/Button"

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => {
    return (
        <div className="button-wrapper">
            {firstText && <Button onClick={firstClick}>{firstText}</Button>}
            {secondaryText && <Button onClick={secondaryClick}>{secondaryText}</Button>}
        </div>
    )
}

// ModalFooter.propTypes = {
//     firstText: PropTypes.string,
//     secondaryText: PropTypes.string,
//     firstClick: PropTypes.func,
//     secondaryClick: PropTypes.func
// }

export default ModalFooter