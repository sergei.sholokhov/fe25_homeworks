import { createSlice } from "@reduxjs/toolkit";

const cartItemsFromStorage = localStorage.getItem('cartItems');
const initialCartItems = cartItemsFromStorage ? JSON.parse(cartItemsFromStorage) : [];

const initialState = {
    cartItems: initialCartItems,
    cartCount: initialCartItems.length
}

const cartItemsSlice = createSlice({
    name: "cartItems",
    initialState,
    reducers: {
        addToCart: (state, { payload }) => {
            const existsIndex = state.cartItems.findIndex(item => item.article === payload.article)

            if (existsIndex < 0) {
                const buildCartItem = { ...payload };
                state.cartItems.push(buildCartItem);
                localStorage.setItem('cartItems', JSON.stringify(state.cartItems))
                state.cartCount += 1;
            } else {
                alert(`${payload.name} is already in Cart`)
            }
        },

        removeFromCart: (state, { payload }) => {
            const itemIndex = state.cartItems.findIndex(
                (item) => item.article === payload.article
            );

            if (itemIndex >= 0) {
                state.cartItems.splice(itemIndex, 1);
                state.cartCount -= 1;

                localStorage.setItem("cartItems", JSON.stringify(state.cartItems));
            } else {
                alert("Item not found in cart");
            }
        },
    }
})

export const { addToCart: addToCartAction, removeFromCart: removeFromCartAction } = cartItemsSlice.actions

export default cartItemsSlice.reducer;