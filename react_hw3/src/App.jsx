import { useState, useEffect, useCallback } from "react";
import { Routes, Route } from 'react-router-dom'
import Header from "./components/header/Header";
import ProductList from "./components/products/ProductList";
import Cart from './components/cart/Cart';
import Favorites from './components/favorites/Favorites';
import './index.scss'

const App = () => {
    const [isReady, setIsReady] = useState(false)
    const [products, setProducts] = useState([]);
    const [favorites, setFavorites] = useState([]);
    const [cartItems, setCartItems] = useState([]);
    const [favoritesCount, setFavoritesCount] = useState(0);
    const [cartCount, setCartCount] = useState(0);
    const [deleteModalOpen, setDeleteModalOpen] = useState(false);
    const [deleteArticle, setDeleteArticle] = useState(null);
    const [modalProductArticle, setModalProductArticle] = useState(false);

    useEffect(() => {
        fetch("/products.json")
            .then((response) => response.json())
            .then((data) => setProducts(data.products))
            .catch((error) => console.error("Error fetching products:", error));
    }, []);

    useEffect(() => {
        const favoritesParsed = JSON.parse(localStorage.getItem("favorites")) || [];
        const cartItemsParsed = JSON.parse(localStorage.getItem("cartItems")) || [];
        setFavoritesCount(favoritesParsed.length);
        setCartCount(cartItemsParsed.length);
        setFavorites(favoritesParsed);
        setCartItems(cartItemsParsed);
        setIsReady(true)
    }, [favoritesCount, cartCount]);

    const modalProduct = products.find(
        (p) => modalProductArticle && p.article === modalProductArticle
    );

    const handleAddToCart = () => {
        const cartItemsInStorage = JSON.parse(localStorage.getItem("cartItems")) || [];
        const existingProductIndex = cartItemsInStorage.findIndex(item => item.article === modalProduct.article);

        if (existingProductIndex !== -1) {
            alert(`You already have ${modalProduct.name} item in your cart`);
        } else {
            cartItemsInStorage.push({
                article: modalProduct.article,
                name: modalProduct.name,
                price: modalProduct.price,
                image: modalProduct.image,
                color: modalProduct.color
            });
        }
        localStorage.setItem("cartItems", JSON.stringify(cartItemsInStorage));

        setCartCount(cartItemsInStorage.length);
    };

    const handleDeleteFromCart = (article) => {
        setDeleteModalOpen(true);
        setDeleteArticle(article);
    };

    const handleDeleteConfirmation = () => {
        const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
        const updatedCartItems = cartItems.filter(item => item.article !== deleteArticle);
        localStorage.setItem('cartItems', JSON.stringify(updatedCartItems));
        setDeleteModalOpen(false);
        setCartCount(updatedCartItems.length);
    };

    const toggleFavorite = useCallback((product) => {
        const updatedProducts = products.map((p) => {
            if (p.article === product.article) {
                return { ...p, isFavorite: !p.isFavorite };
            }
            return p;
        });
        setProducts(updatedProducts);

        const favoritesInStorage = JSON.parse(localStorage.getItem("favorites")) || [];
        if (!product.isFavorite) {
            favoritesInStorage.push({
                article: product.article,
                name: product.name,
                price: product.price,
                image: product.image,
                color: product.color

            });
            localStorage.setItem("favorites", JSON.stringify(favoritesInStorage));
        } else {
            const index = favoritesInStorage.findIndex(
                (favorite) => favorite.article === product.article
            );
            favoritesInStorage.splice(index, 1);
            localStorage.setItem("favorites", JSON.stringify(favoritesInStorage));
        }

        setFavoritesCount(favoritesInStorage.length);
    }, [products, setFavoritesCount]);

    const productsWithFavorites = products.map((d) => {
        const favouriteProduct = favorites.find((f) => {
            return f.article === d.article
        })
        return {
            ...d, isFavorite: !!favouriteProduct
        }
    })

    const cartWithFavorites = cartItems.map((c) => {
        const cardFavorites = favorites.find((f) => {
            return f.article === c.article
        })
        return {
            ...c, isFavorite: !!cardFavorites
        }
    })

    const favoritesStar = favorites.map((f) => {
        return { ...f, isFavorite: true }
    })

    return (
        <div className="app-container">
            <Header
                cartCount={cartCount}
                favoritesCount={favoritesCount}
            />
            <Routes>
                <Route path='/'
                    element={isReady ? (
                        <ProductList
                            modalProduct={modalProduct}
                            setModalProductArticle={setModalProductArticle}
                            modalProductArticle={modalProductArticle}
                            handleAddToCart={handleAddToCart}
                            toggleFavorite={toggleFavorite}
                            productsWithFavorites={productsWithFavorites}
                            deleteModalOpen={deleteModalOpen}
                            setDeleteModalOpen={setDeleteModalOpen}
                            handleDeleteFromCart={handleDeleteFromCart}
                            handleDeleteConfirmation={handleDeleteConfirmation}
                        />) : null}
                />

                <Route path='/favorites' element={isReady ? (
                    <Favorites
                        // favorites={favorites}
                        favoritesStar={favoritesStar}
                        modalProduct={modalProduct}
                        setModalProductArticle={setModalProductArticle}
                        modalProductArticle={modalProductArticle}
                        handleAddToCart={handleAddToCart}
                        toggleFavorite={toggleFavorite}
                    />) : null}>
                </Route>

                <Route path='/cart'
                    element={isReady ? (
                        <Cart
                            // cartItems={cartItems}
                            cartWithFavorites={cartWithFavorites}
                            toggleFavorite={toggleFavorite}
                            deleteModalOpen={deleteModalOpen}
                            setDeleteModalOpen={setDeleteModalOpen}
                            handleDeleteFromCart={handleDeleteFromCart}
                            handleDeleteConfirmation={handleDeleteConfirmation}
                        />) : null}
                />
            </Routes>
        </div>
    );
};

export default App;


