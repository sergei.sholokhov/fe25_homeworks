import React from 'react';
import StarIcon from './StarIcon';

const SvgIcon = () => (
  <span className="svg-icon">
    <StarIcon />
  </span>
);

export default SvgIcon;
