import React from 'react';
import PropTypes from 'prop-types';
import Product from './../products/Product';
import ModalBase from '../modal/ModalBase';
import './../products/ProductList.scss';

const Cart = ({
    cartItems,
    cartWithFavorites,
    toggleFavorite,
    deleteModalOpen,
    setDeleteModalOpen,
    handleDeleteFromCart,
    handleDeleteConfirmation
}) => {
    return (
        <>
            <div className="cart">
                <h2>Shopping Cart</h2>
                <div className="product-grid">
                    {cartWithFavorites.map((product) => (
                        <Product
                            key={product.article}
                            product={product}
                            toggleFavorite={() => toggleFavorite(product)}
                            handleDelete={() => handleDeleteFromCart(product.article)}
                            openDeleteModal={() => handleDeleteFromCart(product.article)}
                            add
                        />
                    ))}
                </div>
            </div>

            {deleteModalOpen && (
                <ModalBase
                    isOpen={deleteModalOpen}
                    title="Confirmation"
                    description={`Are you sure you want to delete this item from your cart?`}
                    handleClose={() => setDeleteModalOpen(false)}
                    handleOk={handleDeleteConfirmation}
                    handleCancel={() => setDeleteModalOpen(false)}
                    isSecondModalToggle={true}
                />
            )}
        </>

    );
};

Cart.propTypes = {
    cartWithFavorites: PropTypes.array,
    toggleFavorite: PropTypes.func,
    deleteModalOpen: PropTypes.bool,
    setDeleteModalOpen: PropTypes.any, 
    handleDeleteFromCart: PropTypes.func, 
    handleDeleteConfirmation: PropTypes.func
}

export default Cart;
