import React from 'react';
import PropTypes from 'prop-types';
import Product from './../products/Product';
import ModalBase from '../modal/ModalBase';
import ProductDescription from "../products/ProductDescription";
import './../products/ProductList.scss';

const Favorites = ({
    // favorites,
    favoritesStar,
    toggleFavorite,
    modalProduct,
    setModalProductArticle,
    modalProductArticle,
    handleAddToCart
}) => {

    return (
        <>
            <div className="favorites">
                <h2>Favorites</h2>
                <div className="product-grid">
                    {favoritesStar.map((product) => (
                        <Product
                            key={product.article}
                            product={product}
                            toggleFavorite={() => toggleFavorite(product)}
                            onModal={() => setModalProductArticle(product.article)}
                            cross
                        />
                    ))}
                </div>
            </div>

            {modalProduct && (
                <ModalBase
                    isOpen={!!modalProductArticle}
                    title={modalProduct.name}
                    description={<ProductDescription desc={modalProduct} />}
                    handleClose={() => {
                        setModalProductArticle(false);
                    }}
                    handleOk={handleAddToCart}
                    isSecondModalToggle={false}
                />
            )}

        </>
    );
};

Favorites.propTypes = {
    favoritesStar: PropTypes.array,
    toggleFavorite: PropTypes.func,
    modalProduct: PropTypes.object,
    setModalProductArticle: PropTypes.func,
    modalProductArticle: PropTypes.any,
    handleAddToCart: PropTypes.func
}

export default Favorites;

