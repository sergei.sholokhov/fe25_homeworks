import React from "react";
import PropTypes from 'prop-types';
import Product from "./Product";
import ModalBase from "./../modal/ModalBase";
import ProductDescription from "../products/ProductDescription"
import "./ProductList.scss";

const ProductList = ({
    handleDeleteFromCart,
    handleDeleteConfirmation,
    deleteModalOpen,
    setDeleteModalOpen,
    modalProduct,
    setModalProductArticle,
    modalProductArticle,
    handleAddToCart,
    toggleFavorite,
    productsWithFavorites
}) => {

    return (
        <>
            <div className="product-grid">
                {productsWithFavorites.map((product) => (
                    <Product
                        key={product.article}
                        product={product}
                        toggleFavorite={() => toggleFavorite(product)}
                        onModal={() => setModalProductArticle(product.article)}
                        handleDelete={() => handleDeleteFromCart(product.article)}
                        openDeleteModal={() => handleDeleteFromCart(product.article)}
                    />
                ))}
            </div>

            {modalProduct && (
                <ModalBase
                    isOpen={!!modalProductArticle}
                    title={modalProduct.name}
                    description={<ProductDescription desc={modalProduct} />}
                    handleClose={() => {
                        setModalProductArticle(false);
                    }}
                    handleOk={handleAddToCart}
                    isSecondModalToggle={false}
                />
            )}

            {deleteModalOpen && (
                <ModalBase
                    isOpen={deleteModalOpen}
                    title="Confirmation"
                    description={`Are you sure you want to delete this item from your cart?`}
                    handleClose={() => setDeleteModalOpen(false)}
                    handleOk={handleDeleteConfirmation}
                    handleCancel={() => setDeleteModalOpen(false)}
                    isSecondModalToggle={true}
                />
            )}
        </>
    );
};

ProductList.propTypes = {
    handleDeleteFromCart: PropTypes.func,
    handleDeleteConfirmation: PropTypes.func,
    deleteModalOpen: PropTypes.bool,
    setDeleteModalOpen: PropTypes.func,
    modalProduct: PropTypes.object,
    setModalProductArticle: PropTypes.func,
    modalProductArticle: PropTypes.any,
    handleAddToCart: PropTypes.func,
    toggleFavorite: PropTypes.func,
    productsWithFavorites: PropTypes.array    
}

export default ProductList;

