import React from "react";
import PropTypes from 'prop-types'

const ProductDescription = ({ desc }) => {
    return (
        <>
            <p>color: {desc.color}</p>
            <p>price: {desc.price}</p>
        </>
    )
}

ProductDescription.propTypes = {
    desc: PropTypes.object
}

export default ProductDescription;